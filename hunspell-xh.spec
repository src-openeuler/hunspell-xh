Name:        hunspell-xh
Summary:     Dictionaries for Xhosa hunspell
Version:     0.20091030
Release:     16
Source:      https://downloads.sourceforge.net/project/aoo-extensions/3133/0/dict-xh_za-2009.10.30.oxt
URL:         https://extensions.openoffice.org/en/project/xhosa-spell-checker
License:     LGPLv2+
BuildArch:   noarch
Requires:    hunspell
Supplements: (hunspell and langpacks-xh)

%description
Dictionaries for Xhosa hunspell.

%prep
%autosetup -c -n hunspell-xh -p1

%build
for i in README-xh_ZA.txt release-notes-xh_ZA.txt package-description.txt; do
  if ! iconv -f utf-8 -t utf-8 -o /dev/null $i > /dev/null 2>&1; then
    iconv -f ISO-8859-2 -t UTF-8 $i > $i.new; touch -r $i $i.new; mv -f $i.new $i
  fi
  tr -d '\r' < $i > $i.new; touch -r $i $i.new; mv -f $i.new $i
done

%install
install -d $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%files
%doc README-xh_ZA.txt release-notes-xh_ZA.txt package-description.txt
%{_datadir}/myspell/*

%changelog
* Wed Apr 29 2020 wutao <wutao61@huawei.com> - 0.20091030-16
- Package init
